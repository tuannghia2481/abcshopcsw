package com.example.cswpratice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CswPraticeApplication {

    public static void main(String[] args) {
        SpringApplication.run(CswPraticeApplication.class, args);
    }

}
