package com.example.cswpratice.repository;

import com.example.cswpratice.entity.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product,Integer> {
}
